#!/bin/sh
CITY="BRISBANE"

# curl wttr.in/$CITY?format=3
curl -s wttr.in/$CITY?format="%c%t\n"

# See https://github.com/chubin/wttr.in for format info
    # c    Weather condition,
    # C    Weather condition textual name,
    # h    Humidity,
    # t    Temperature (Actual),
    # f    Temperature (Feels Like),
    # w    Wind,
    # l    Location,
    # m    Moonphase 🌑🌒🌓🌔🌕🌖🌗🌘,
    # M    Moonday,
    # p    precipitation (mm),
    # P    pressure (hPa),

    # D    Dawn*,
    # S    Sunrise*,
    # z    Zenith*,
    # s    Sunset*,
    # d    Dusk*.