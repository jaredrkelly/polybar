SLEEP_INTERVAL="5"

IFS=$'\n'
while true; do
    RESPONSE=$(curl -sA 'agent' https://www.reddit.com/r/worldnews/hot/.json)
    TITLES=$( jq -r '.data.children[].data.title' <<< "${RESPONSE}" )
    readarray -t LINKS <<< $( jq -r '.data.children[].data.url' <<< "${RESPONSE}" )
    readarray -t REDDIT_LINKS <<< $( jq -r '.data.children[].data.permalink' <<< "${RESPONSE}" )

    i=0
    for TITLE in ${TITLES[@]}; do
        echo "$TITLE"
        echo ${LINKS[$i]} > link.txt
        echo "old.reddit.com${REDDIT_LINKS[$i]}" > reddit_link.txt
        sleep $SLEEP_INTERVAL
        ((i++))
    done
done